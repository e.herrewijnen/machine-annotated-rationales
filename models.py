import pandas as pd
import numpy as np
import torch.optim as optim
import torch
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
import rationale_quality
import plots

class BagOfSentences(nn.Module):
    def __init__(self):
        super(BagOfSentences, self).__init__()
        self.flat = nn.Flatten()
        self.conv1 = nn.Conv1d(768, 768, kernel_size=(1))
        self.conv2 = nn.Conv1d(768, 768, kernel_size=(1))
        self.conv3 = nn.Conv1d(768, 768, kernel_size=(1))

        self.pool = nn.MaxPool1d(1)
        self.lin = nn.Linear(768, 768)
        self.lin4 = nn.Linear(768, 1)
        self.lin = nn.Linear(768, 768)


    def forward(self, x):
        x = torch.transpose(x, 1, 2)

        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = torch.transpose(x, 1, 2)
        x = self.lin4(x)
        x = self.flat(x)
        return x

    def name(self):
        return "BagOfSentences"

    def isRats(self):
        return False

    def predict(self, input):
        pred = torch.sigmoid(self(input))
        return pred.mean()

class BagOfRationales(nn.Module):
    def __init__(self):
        super(BagOfRationales, self).__init__()
        self.flat = nn.Flatten()
        self.conv1 = nn.Conv1d(768, 768, kernel_size=(1))
        self.conv2 = nn.Conv1d(768, 768, kernel_size=(1))
        self.conv3 = nn.Conv1d(768, 768, kernel_size=(1))

        self.pool = nn.MaxPool1d(1)
        self.lin = nn.Linear(768, 768)
        self.lin4 = nn.Linear(768, 1)
        self.metric = 0

    def forward(self, x):
        x = torch.transpose(x, 1, 2)
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = torch.transpose(x, 1, 2)
        x = self.lin4(x)
        x = self.flat(x)
        return x

    def name(self):
        return "BagOfRationales"

    def isRats(self):
        return True

    def predict(self, input):
        pred = torch.sigmoid(self(input))
        logits = getRationalesFromLogits(pred.squeeze(), [], pred.mean(1).item() )
        # Use prediction considereing only the MRs
        return logits.mean()


class Dataset(torch.utils.data.Dataset):
  'Characterizes a dataset for PyTorch'
  def __init__(self, dataset):
        'Initialization'
        self.labels = torch.from_numpy(np.array(dataset["tag"].values,  dtype="float"))
        self.vectors = torch.from_numpy(np.stack(dataset["padded_vectors"]))

  def __len__(self):
        'Denotes the total number of samples'
        return len(self.labels)

  def __getitem__(self, index):
      return self.vectors[index], self.labels[index], index


def getRationalesFromLogits(sentence_logits, sents, prediction):
    """Find sentences that form rationales for a prediction using logits.

    Parameters
    ----------
    sentence_logits : [float]
        Output of the model, logits for every sentence in the document.
    sents : [String]
        The text sentences in the document.
    prediction : float
        The prediction of the whole document.

    Returns
    -------
    [float], [String]

        When sentences is not empty, return a list of logits and
        a list of according sentences that form rationales.
        When sentences is empty, return all logits.

    """
    masked_predictions = sentence_logits.cpu().detach()
    pos_classification = prediction < 0.5
    p = 0.2
    bins = round(1 / p)
    last_bin = 1 if pos_classification else (bins - 1)
    l = np.histogram(masked_predictions, bins=bins)[1][last_bin]
    bound = masked_predictions <= l if pos_classification else masked_predictions >= l

    if sents != []:
        sentence_ids = list(map(str, list(range(len(sents)))))
        data = list(zip(sentence_logits, sents, sentence_ids))
        selected = [x for i,x in enumerate(data) if bound[i]]
        if selected == []:
            selected = data #If no rationales are found, just use the whole set
        logits = torch.stack([x[0] for x in selected])
        mrs = [x[1] for x in selected]
        return logits, mrs
    else:
        # If no sentences are available, return all logits
        selected = [x for i,x in enumerate(sentence_logits) if bound[i]]
        if selected == []:
            selected = list(sentence_logits) #If no rationales are found, just use the whole set
        return torch.stack(selected), []


def validation(model, testloader, criterion, testset, padding, device, verbose = False, store_quality_df = False, is_rats_model = None):
    """The validation function.

    Parameters
    ----------
    model : BagOfSentences | BagOfRationales
        An instance of BagOfSentences or BagOfRationales.
    testloader : DataLoader
        The dataloader containing the test data.
    criterion : type
        An instance of the criterion object.
    testset : DataFrame
        The dataset used in the testloader.
    padding : tensor
        A tensor used to represent an empty sentence.
    device : device
        The device to perform validation on.
    verbose : bool
        Boolean that determines whether the output should be printed. Defaults to False.
    store_quality_df : bool
        Boolean that determines whether to store the results. Defaults to False.
    is_rats_model : bool
        Boolean that determines how the final prediction should be made. True for BagOfRatinales.
        If None, the result of the isRats() function in the model will be used. Defaults to None.

    Returns
    -------
    tuple
        A tuple containing loss, accuracy, jaccard, completeness (precision),
        incompleteness (recall), comprehensiveness, sufficiency, and complete_quality_df.

    """
    if is_rats_model == None :
        if model.isRats():
            is_rats_model = True
        else:
            is_rats_model = False
    test_loss = 0
    correct = 0
    total = 0
    jaccard = 0
    completeness  = 0
    incompleteness = 0
    LAO_confidence = 0
    complete_quality_df = pd.DataFrame()
    # found_mrs = []
    for i, data in enumerate(testloader):
        inputs, classes, indexes = data
        indexes = np.array(indexes)
        classes = classes.float()
        inputs, classes =  inputs.to(device), classes.to(device)
        outputs = model(inputs)
        outputs_sigmoid = torch.sigmoid(outputs)
        predictions = outputs_sigmoid.mean(-1)
        ars = testset["rationale_sentences"][indexes]
        sentences = testset["sentences"][indexes]
        df = pd.DataFrame(list(zip(outputs_sigmoid, predictions, ars, sentences)), columns = ["sentence_logits", "prediction", "rationale_sentences", "sentences"])
        logits, mrs = zip(*df.apply(lambda x: getRationalesFromLogits(x.sentence_logits, x.sentences, x.prediction.item() ), axis=1))

        if is_rats_model:
            predictions = torch.stack(list(map(lambda x : x.mean(), logits)))
        test_loss += float(criterion(predictions, classes).item())

        LAO_preds = rationale_quality.getLAOPredictions(testset.iloc[indexes].reset_index(), model, mrs, padding, device)
        quality_df = rationale_quality.calculateQuality(ars, mrs, np.array(classes.cpu()), predictions.cpu().detach().numpy(), LAO_preds, verbose = verbose)
        complete_quality_df = complete_quality_df.append(quality_df, ignore_index = True) if store_quality_df else complete_quality_df
        quality = quality_df["jaccard"].mean(), quality_df["completeness"].mean(), quality_df["incompleteness"].mean(), quality_df["diff_LAO_preds"].mean()

        jaccard += float(quality[0])
        completeness += float(quality[1])
        incompleteness += float(quality[2])
        LAO_confidence += float(quality[3])
        r_outputs = (predictions > 0.5).float()
        correct += float((r_outputs == classes).sum())
        total += int(len(classes))
        del inputs
        del classes
        del outputs
        del r_outputs
        del outputs_sigmoid
        del predictions
        del logits
        del indexes
        del LAO_preds
    accuracy = (correct / total)
    test_loss =  test_loss / (i + 1)
    jaccard = jaccard / (i + 1)
    completeness = completeness / (i + 1)
    incompleteness = incompleteness / (i + 1)
    LAO_confidence = LAO_confidence /  (i + 1)
    return test_loss, accuracy, jaccard, completeness, incompleteness, LAO_confidence, complete_quality_df


def train(model, criterion, optimizer, traindataLoader, testdataLoader, testset, device, padding = None, epochs = 30, calc_rationale_quality = True, is_rats_model = None):
    """The train function.

    Parameters
    ----------
    model : BagOfSentences | BagOfRationales
        An instance of BagOfSentences or BagOfRationales.

    criterion : type
        An instance of the criterion object.
    optimizer : type
        An instance of the optimizer object.
    traindataLoader : type
        The dataloader containing the train data.
    testloader : DataLoader
        The dataloader containing the test data.
    testset : DataFrame
        The dataset used in the testloader.
    device : device
        The device to perform validation on.
    padding : tensor
        A tensor used to represent an empty sentence.
    epochs: int
        Number of epoch to perform. Default is 30.
    calc_rationale_quality : type
        Boolean that determines whether the quality of the found rationales should be evaluated.
        Defaults to True.
    is_rats_model : bool
        Boolean that determines how the final prediction should be made. True for BagOfRatinales.
        If None, the result of the isRats() function in the model will be used. Defaults to None.

    Returns
    -------
    [tuple]
        A list of tuples containing epoch, train-loss, train-accuracy, test-loss, test-accuracy, jaccard, completeness (precision),
        incompleteness (recall), comprehensiveness, sufficiency, and complete_quality_df.

    """

    if is_rats_model == None :
        if model.isRats():
            is_rats_model = True
        else:
            is_rats_model = False
    test_stats = []
    for epoch in range(epochs):  # loop over the dataset multiple times
        running_loss = 0.0
        correct = 0
        total = 0
        for i, data in enumerate(traindataLoader):
            inputs, labels = data[0], data[1]
            labels = labels.float()
            inputs, labels = inputs.to(device), labels.to(device)
            # forward + backward + optimize
            outputs = model(inputs)
            outputs_sigmoid = torch.sigmoid(outputs)
            #Find MRs using the average of this document
            mean_outputs = outputs_sigmoid.mean(-1)
            if is_rats_model:
                df = pd.DataFrame(list(zip(outputs_sigmoid, mean_outputs)), columns = ["sentence_logits", "prediction"])
                logits, sents =  zip(*df.apply(lambda x: getRationalesFromLogits(x.sentence_logits, [], x.prediction.item() ), axis=1))
                #Use prediction considereing only the MRs
                mean_outputs = torch.stack(list(map(lambda x : x.mean(), logits)))
                loss = criterion(mean_outputs, labels)
                del logits
            else:
                loss = criterion(mean_outputs, labels)
            # zero the parameter gradients
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # Print statistics
            r_outputs = (mean_outputs > 0.5).float()
            correct += (r_outputs == labels).sum().float()
            total += len(labels)
            running_loss += float(loss.item())
            del inputs
            del labels
            del outputs
            del outputs_sigmoid
            del mean_outputs

        running_loss = running_loss / (i + 1)
        accuracy = (correct / total).item()
        print('Training: [%d, %5d] loss: %.3f accuracy: %.3f' %
              (epoch + 1, i + 1, running_loss, accuracy))
        test_vals = validation(model, testdataLoader, criterion, testset, padding, device, is_rats_model = is_rats_model)

        test_stats.append(((epoch, running_loss, accuracy,) + test_vals))
        print("Test:\n  Loss: %.3f Accuracy: %.3f" % (test_vals[0], test_vals[1]))
    return test_stats

if __name__ == '__main__':
    traindata = pd.read_pickle("data/traindata.pickle")
    testdata_holdout = pd.read_pickle("data/testdata_holdout.pickle")
    testdata = pd.read_pickle("data/testdata.pickle")
    evaldata = pd.read_pickle("data/evaldata.pickle")
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    batch_size = 32
    traindataset = Dataset(traindata)
    traindataLoader = torch.utils.data.DataLoader(traindataset , batch_size=batch_size, )

    testholdoutDataset = Dataset(testdata_holdout)
    testholdoutLoader = torch.utils.data.DataLoader(testholdoutDataset , batch_size=batch_size, )

    testDataset = Dataset(testdata)
    testdataLoader = torch.utils.data.DataLoader(testDataset , batch_size=batch_size, )

    evalDataset = Dataset(evaldata)
    evaldataLoader = torch.utils.data.DataLoader(evalDataset , batch_size=batch_size, )

    model = BagOfRationales()
    model = model.to(device)
    optimizer = optim.Adam(model.parameters(), lr=0.0001)
    criterion = nn.BCELoss()
    epochs = 1

    # Create padding
    padding = torch.zeros(768, dtype=torch.float32).unsqueeze(0)
    padding = padding.to(device)

    train_stats = train(model, criterion, optimizer, traindataLoader, testholdoutLoader, testdata_holdout, device, padding, epochs, True)
    plots.plotTrainHistory(train_stats, model.name())

    test_holdout_scores = validation(model, testholdoutLoader, criterion, testdata_holdout, padding, device, store_quality_df = True  )

    test_scores = validation(model, testdataLoader, criterion, testdata, padding, device, store_quality_df = True)
    eval_scores = validation(model, evaldataLoader, criterion, evaldata, padding, device, store_quality_df = True)
