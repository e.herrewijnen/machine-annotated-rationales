import matplotlib.pyplot as plt

def getAndSavePlot(x, y, label, title = "", modelName = "", start = 0, filename = None):
    if isinstance(y, list):
        name = label[0]
        for i, y_series in enumerate(y):
            plt.plot(x, y_series, label=label[i])
            name += label[i] + "_"
    else:
        name = label
        plt.plot(x, y, label=label )
    plt.xlim(left=start)
    plt.xlabel('epoch')
    plt.title(title)
    plt.legend()
    if filename is not None:
        name = filename
    if modelName != "":
        name = name + "_" + modelName

    plt.savefig(name + "plot.png")
    return plt

def plotTrainHistory(trainHistory, modelName = "", start = 0):
    unzipped = list(zip(*trainHistory))
    epoch_series = unzipped[0]
    train_loss_series = unzipped[1]
    train_acc_series = unzipped[2]
    test_loss_series = unzipped[3]
    test_acc_series = unzipped[4]
    jaccard_series = unzipped[5]
    completeness_series = unzipped[6]
    incompleteness_series = unzipped[7]
    confidence_series = unzipped[8]

    getAndSavePlot(epoch_series, [train_loss_series, test_loss_series ], ["train loss", "test loss"], "Loss for " + modelName,  modelName,  start, "loss").show()
    getAndSavePlot(epoch_series, [train_acc_series, test_acc_series], ["train accuracy", "test accuracy"], "Accuracy for " + modelName,  modelName,  start, "accuracy").show()
    getAndSavePlot(epoch_series, jaccard_series, "jaccard", "Jaccard index for " + modelName,  modelName, start).show()
    getAndSavePlot(epoch_series, completeness_series, "completeness", "Completeness for " + modelName,  modelName,  start).show()
    getAndSavePlot(epoch_series, incompleteness_series, "incompleteness", "Incompleteness for " + modelName, modelName,  start).show()
    getAndSavePlot(epoch_series, confidence_series, "LOA confidence", "LAO confidence for " + modelName,  modelName,  start).show()
    getAndSavePlot(epoch_series, [jaccard_series,completeness_series, incompleteness_series, confidence_series], ["jaccard", "completeness", "incompleteness", "LAO confidence"], "Rationale quality for " + modelName, modelName, start, "rationale_quality").show()
