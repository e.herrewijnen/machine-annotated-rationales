import nltk
import nltk.data
import os
import numpy as np
import pandas as pd
import re
import string
from sentence_transformers import SentenceTransformer
import spacy
from spacy.pipeline import SentenceSegmenter
import random

tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
pos_train_doc_path = "./data/train/noRats_pos"
neg_train_doc_path = "./data/train/noRats_neg"
pos_train_rats_doc_path = "./data/train/withRats_pos"

neg_train_rats_doc_path = "./data/train/withRats_neg"
pos_test_rats_doc_path = "./data/dev/withRats_pos"
neg_test_rats_doc_path = "./data/dev/withRats_neg"
pos_test_rats_doc_holdout_path = "./data/dev/withRats_pos/holdout"
neg_test_rats_doc_holdout_path = "./data/dev/withRats_neg/holdout"

pos_eval_doc_path = "./data/evaluation/pos"
neg_eval_doc_path = "./data/evaluation/neg"

pos_custom_doc_path = "./data/custom_rationales/posR"
neg_custom_doc_path = "./data/custom_rationales/negR"

translator = str.maketrans('', '', string.punctuation)
nlp = spacy.load("en_core_web_sm")
sentencizer = SentenceSegmenter([" . ", " ! ", " ? "])
nlp.add_pipe(sentencizer)

def getDataWithRats(directory_in_str, classificationTag, embeddingModel, rationaleTags):
    """Store data from seperate documents in a dataframe.

    Parameters
    ----------
    directory_in_str : string
        The directory containing the documents.
    classificationTag : int or string
        The classification tag of all documents in that directory.
    embeddingModel : SentenceTransformer
        An instance of SentenceTransformer used to encode sentences
    rationaleTags : [String]
        The tags that surrounds rationales in the document.
        Make sure the first element of the rationaleTags is the tag that supports the classificationTag.
        i.e. 0 = POS abd 1 = NEG

    Returns
    -------
    DataFrame
        A pandas DataFrame containing:
        - filename: the name of the document.
        - sentences: a list of sentences in the document.
        - vectors: a list of vectors, one for every sentence in the document.
        - tag: the classification tag of the document.
        - is_rationales_sentences: for every sentence in the document, a boolean whether that sentence is an annotator rationale.
        - annotator_rationales: the list of annotator rationales for the documents that support the classification.
        - rationale_sentences: the list of sentences that contain annotator rationales.
        - random_rationale_sentences: a list of randomly selected sentences that can be used as a random rationale baseline.
        - anti_annotator_rationales: the list of annotator rationales for the documents that support a different classification than the classification in tag.
        - anti_rationale_sentences: the list of sentences that contain anti annotator rationales.

    """
    directory = os.fsencode(directory_in_str)
    df = pd.DataFrame(columns = ["filename", "vectors", "tag"])

    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if os.path.isdir(directory_in_str + "/" + filename): continue
        vectorSentences = []
        openedFile = open(directory_in_str + "/" + filename, "r").read()
        openedFile = re.sub(r'(\. )+', ". ", openedFile)
        openedFile = re.sub(r'( ([\.\,\?\!]))+', r"\1", openedFile)

        annotatorRationales = []
        isRationalesSentences = []
        rationalesSentences =  [[] for _ in range(len(rationaleTags))]
        annotatorRationales =  [[] for _ in range(len(rationaleTags))]
        sentences = [x.text for x in nlp(openedFile).sents]
        adjusted_sentences = []
        for i, sentence in enumerate(sentences):
            sentence = sentence.replace('\n', '')
            isRationalesSentences.append(False)
            # Find rationales for all tags provided in rationaleTags.
            for j, rationaleTag in enumerate(rationaleTags):
                annotatorRationales[j] = re.findall("<"+ rationaleTag + r">(.+?)<\/" + rationaleTag + ">", openedFile);
                isRationale = "<" + rationaleTag +">" in sentence or "</" + rationaleTag +">" in sentence
                sentence = sentence.replace("<" + rationaleTag +">", "")
                sentence = sentence.replace("</"+ rationaleTag +">", "")
                sentence = re.sub(' +', ' ', sentence)
                sentence = sentence[1:] if sentence.startswith(" ") else sentence
                if isRationale:
                    rationalesSentences[j].append(sentence)
                    isRationalesSentences[i] = True
            adjusted_sentences.append(sentence)
        vectorSentences = embeddingModel.encode(adjusted_sentences)
        num_sentences = len(adjusted_sentences)
        sampleSize = random.randint(int(0.1 * num_sentences), int(0.9 * num_sentences))
        randomRationales = random.sample(adjusted_sentences, sampleSize)
        serie = pd.Series([filename, adjusted_sentences, vectorSentences, classificationTag, isRationalesSentences, annotatorRationales[0], rationalesSentences[0], randomRationales, annotatorRationales, rationalesSentences, ], index=["filename", "sentences", "vectors", "tag", "is_rationales_sentences", "annotator_rationales", "rationale_sentences", "random_rationale_sentences", "anti_annotator_rationales", "anti_rationale_sentences" ])
        df = df.append(serie, ignore_index=True)
    return df

if __name__ == '__main__':

    embeddingModel = SentenceTransformer('bert-base-nli-mean-tokens')

    customPosRationales = getDataWithRats(pos_custom_doc_path, 0, embeddingModel, ["POS","NEG"])
    customNegRationales = getDataWithRats(neg_custom_doc_path, 1, embeddingModel, ["NEG", "POS"])
    customRationales = pd.concat([customPosRationales, customNegRationales], ignore_index = True)

    traindatapos = getDataWithRats(pos_train_rats_doc_path, 0, embeddingModel, ["POS"])
    traindataneg = getDataWithRats(neg_train_rats_doc_path, 1, embeddingModel, ["NEG"])
    traindata = pd.concat([traindatapos, traindataneg], ignore_index = True)
    traindata = traindata.sample(frac=1).reset_index(drop=True) #shuffle set

    testdatapos = getDataWithRats(pos_test_rats_doc_path, 0, embeddingModel, ["POS"])
    testdataneg = getDataWithRats(neg_test_rats_doc_path, 1, embeddingModel, ["NEG"])
    testdata = pd.concat([testdatapos, testdataneg], ignore_index = True)

    testdataholdoutpos = getDataWithRats(pos_test_rats_doc_holdout_path, 0, embeddingModel, ["POS"])
    testdataholdoutneg = getDataWithRats(neg_test_rats_doc_holdout_path, 1, embeddingModel, ["NEG"])
    testdataholdout = pd.concat([testdataholdoutpos, testdataholdoutneg], ignore_index = True)

    evaldatapos = getDataWithRats(pos_eval_doc_path, 0, embeddingModel, ["POS"])
    evaldataneg = getDataWithRats(neg_eval_doc_path, 1, embeddingModel, ["NEG"])
    evaldata = pd.concat([evaldatapos, evaldataneg], ignore_index = True)

    maxSentenceLevel = max(testdataholdout['vectors'].apply(lambda v:  len(v)))
    traindata['padded_vectors'] = traindata['vectors'].apply(lambda x: np.pad(x, ((0, maxSentenceLevel - len(x)), (0,0)), 'constant', constant_values=(0)))
    testdata['padded_vectors'] = testdata['vectors'].apply(lambda x: np.pad(x, ((0, maxSentenceLevel - len(x)), (0,0)), 'constant', constant_values=(0)))
    testdataholdout['padded_vectors'] = testdataholdout['vectors'].apply(lambda x: np.pad(x, ((0, maxSentenceLevel - len(x)), (0,0)), 'constant', constant_values=(0)))
    evaldata['padded_vectors'] = evaldata['vectors'].apply(lambda x: np.pad(x, ((0, maxSentenceLevel - len(x)), (0,0)), 'constant', constant_values=(0)))
    customRationales['padded_vectors'] = customRationales['vectors'].apply(lambda x: np.pad(x, ((0, maxSentenceLevel - len(x)), (0,0)), 'constant', constant_values=(0)))

    customRationales.to_pickle('data/customRationales.pickle')
    traindata.to_pickle('data/traindata.pickle')
    testdata.to_pickle('data/testdata.pickle')
    testdataholdout.to_pickle('data/testdata_holdout.pickle')
    evaldata.to_pickle('data/evaldata.pickle')
