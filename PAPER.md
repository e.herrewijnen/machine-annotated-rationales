### \# Rationales
| |   mean| median  |  mode | min  | max|
|---|---|---|---|---|---|
|**random**|17.095|15|10|0|68|
|**AR** |7.09|7|7|1|22|
|**BoS**|6.11|5|3|1|25|
|**BoR**|5.47|5|4|1|24|

Table 1: Average number of rationales selected in the
random rationale, annotator rationale, BoS rationale, and BoR rationale sets.
