import pandas as pd
import numpy as np
import torch

def intersection(lst1, lst2):
    return [value for value in lst1 if value in lst2]

def union(lst1, lst2):
    return list(set().union(lst1, lst2))

def jaccard_index(lst1, lst2):
    u = union(lst1, lst2)
    if len(u) == 0:
        return 0
    return len(intersection(lst1, lst2)) / len(u)

def incompleteness(annotatorRationales, machineRationales):
    if len(annotatorRationales) == 0:
        return 0
    return 1 - (len(intersection(machineRationales, annotatorRationales)) / len(annotatorRationales))

def completeness(annotatorRationales, machineRationales):
    if len(machineRationales) == 0:
        return 0
    return len(intersection(machineRationales, annotatorRationales)) / len(machineRationales)


def getLAIPredictions(dataset, model, mrs, padding, device):
    preds_with_only_mrs = []
    for i, data in dataset.iterrows():
        c_mrs = mrs[i]
        vectors = np.array(data["padded_vectors"])
        sentences = data["sentences"]
        pred = getLAIPrediction(vectors, sentences, model, c_mrs, padding, device)
        preds_with_only_mrs.append(float(pred))
    return preds_with_only_mrs


def getLAIPrediction(vectors, sentences, model, mrs, padding, device):
    vectors = np.array(vectors)
    for j, sentence in enumerate(sentences):
        if sentence not in mrs:
            vectors[j] = np.array(padding.cpu())
    return model.predict(torch.from_numpy(vectors).unsqueeze(0).to(device))



def getLAOPredictions(dataset, model, mrs, padding, device):
    preds_without_mrs = []
    for i, data in dataset.iterrows():
        c_mrs = mrs[i]
        vectors = np.array(data["padded_vectors"])
        sentences = data["sentences"]
        pred = getLAOPrediction(vectors, sentences, model, c_mrs, padding, device)
        preds_without_mrs.append(float(pred))
    return preds_without_mrs


def getLAOPrediction(vectors, sentences, model, mrs, padding, device):
    vectors = np.array(vectors)
    for j, sentence in enumerate(sentences):
        if sentence in mrs:
            vectors[j] = np.array(padding.cpu())
    return model.predict(torch.from_numpy(vectors).unsqueeze(0).to(device))



def calculateConfidence(predictions, a_predictions, norm_val, dataset, metric_name):
    if a_predictions != []:
        r_a_predictions = list(map(round, a_predictions))
        dif_preds = [abs(a_i - b_i) for a_i, b_i in zip(predictions, a_predictions)]
        norm_dif_preds = [dif / norm_val for dif in dif_preds]
        dataset[metric_name] = a_predictions
        dataset["r_" + metric_name] = r_a_predictions
        dataset["diff_" + metric_name] = dif_preds
        dataset["norm_diff_" + metric_name] = norm_dif_preds

    else:
        dataset["diff_" + metric_name] = 0
        dataset["norm_diff_" + metric_name] = 0
    return dataset


def calculateQuality(rationale_sentences, machineRationales, tags, preds, LAO_preds = [], LAI_preds = [], LAO_k_preds = [], LAI_k_preds = [], logits_sentences = [], mrs_k = [], verbose = False):
    r_predictions = list(map(round, preds))
    min_pred = min(preds)
    max_pred = max(preds)
    norm_val = (max_pred - min_pred)
    if norm_val == 0:
        norm_val = 1 #Normalize over 1 if all predictions are the same
    norm_prediction = [pred / norm_val for pred in preds]

    dataset = list(zip(rationale_sentences, machineRationales, tags, preds, r_predictions, norm_prediction))
    dataset = pd.DataFrame(dataset, columns= ["annotatorRationales", "machineRationales", "tag", "prediction", "r_prediction", "norm_prediction" ])


    dataset = calculateConfidence(preds, LAO_preds, norm_val, dataset, "LAO_predictions")
    dataset = calculateConfidence(preds, LAI_preds, norm_val, dataset, "LAI_predictions")
    dataset = calculateConfidence(preds, LAO_k_preds, norm_val, dataset, "LAO_k_predictions")
    dataset = calculateConfidence(preds, LAI_k_preds, norm_val, dataset, "LAI_k_predictions")



    correct_classifications = dataset[dataset["tag"] == dataset["r_prediction"]]
    if verbose: print("Correctly classified:  %d / %d (%.2f%%)" % (len(correct_classifications), len(dataset), (len(correct_classifications) / len(dataset))))
    incorrect_classifications = dataset[dataset["tag"] != dataset["r_prediction"]]
    if verbose: print("Incorrectly classified: %d / %d (%.2f%%)" % (len(incorrect_classifications), len(dataset), (len(incorrect_classifications) / len(dataset))))
    if LAO_preds != []:
        if verbose: print("Comprehensiveness mean total:  %f, norm: %f" % (dataset["diff_LAO_predictions"].mean(), dataset["norm_diff_LAO_predictions"].mean()))
        if verbose: print("Comprehensiveness mean correctly classified:  %f, norm: %f" % (correct_classifications["diff_LAO_predictions"].mean(), correct_classifications["norm_diff_LAO_predictions"].mean()))
        if verbose: print("Comprehensiveness mean incorrectly classified:  %f, norm: %f" % (incorrect_classifications["diff_LAO_predictions"].mean(), incorrect_classifications["norm_diff_LAO_predictions"].mean()))
        LAO_correct_classifications = dataset[dataset["tag"] != dataset["r_LAO_predictions"]]
        if verbose: print("Comprehensiveness correctly classified: %d / %d (%.2f%%)" % (len(LAO_correct_classifications), len(dataset), (len(LAO_correct_classifications) / len(dataset))))
        if verbose: print("Comprehensiveness incorrectly classified: %d / %d (%.2f%%)" % ((len(dataset) - len(LAO_correct_classifications)), len(dataset), ((len(dataset) - len(LAO_correct_classifications)) / len(dataset))))
    if LAI_preds != []:
        if verbose: print("Sufficiency mean total:  %f, norm: %f" % (dataset["diff_LAI_predictions"].mean(), dataset["norm_diff_LAI_predictions"].mean()))
        if verbose: print("Sufficiency mean correctly classified:  %f, norm: %f" % (correct_classifications["diff_LAI_predictions"].mean(), correct_classifications["norm_diff_LAI_predictions"].mean()))
        if verbose: print("Sufficiency mean incorrectly classified:  %f, norm: %f" % (incorrect_classifications["diff_LAI_predictions"].mean(), incorrect_classifications["norm_diff_LAI_predictions"].mean()))
        LAI_correct_classifications = dataset[dataset["tag"] != dataset["r_LAI_predictions"]]
        if verbose: print("Sufficiency correctly classified: %d / %d (%.2f%%)" % (len(LAI_correct_classifications), len(dataset), (len(LAI_correct_classifications) / len(dataset))))
        if verbose: print("Sufficiency incorrectly classified: %d / %d (%.2f%%)" % ((len(dataset) - len(LAI_correct_classifications)), len(dataset), ((len(dataset) - len(LAI_correct_classifications)) / len(dataset))))


    if verbose: print("Jaccard index:")
    jaccard_total = dataset.apply(lambda x: round(jaccard_index(x.annotatorRationales, x.machineRationales), 3), axis=1)
    dataset["jaccard"] = jaccard_total
    if verbose: print("mean total set: %f" % jaccard_total.mean())
    jaccard_correct = correct_classifications.apply(lambda x: jaccard_index(x.annotatorRationales, x.machineRationales), axis=1)
    if verbose: print("mean correct set: %f" % jaccard_correct.mean() if len(correct_classifications) > 0 else 0)
    jaccard_incorrect = incorrect_classifications.apply(lambda x: jaccard_index(x.annotatorRationales, x.machineRationales),  axis=1)
    if verbose: print("mean incorrect set: %f" % jaccard_incorrect.mean() if len(incorrect_classifications) > 0 else 0)


    if verbose: print("Completeness:")
    completeness_total = dataset.apply(lambda x: round(completeness(x.annotatorRationales, x.machineRationales), 3),  axis=1)
    dataset["completeness"] = completeness_total
    if verbose: print("mean total set: %f" % completeness_total.mean())
    completeness_correct = correct_classifications.apply(lambda x: completeness(x.annotatorRationales, x.machineRationales),  axis=1)
    if verbose: print("mean correct set: %f" % completeness_correct.mean() if len(correct_classifications) > 0 else 0)
    completeness_incorrect = incorrect_classifications.apply(lambda x: completeness(x.annotatorRationales, x.machineRationales),  axis=1)
    if verbose: print("mean incorrect set: %f" % completeness_incorrect.mean() if len(incorrect_classifications) > 0 else 0)

    if verbose: print("Incompleteness:")
    incompleteness_total = dataset.apply(lambda x: round(incompleteness(x.annotatorRationales, x.machineRationales), 3),  axis=1)
    dataset["incompleteness"] = incompleteness_total
    if verbose: print("mean total set: %f" % incompleteness_total.mean())
    incompleteness_correct = correct_classifications.apply(lambda x: incompleteness(x.annotatorRationales, x.machineRationales),  axis=1)
    if verbose: print("mean correct set: %f" % incompleteness_correct.mean() if len(correct_classifications) > 0 else 0)
    incompleteness_incorrect = incorrect_classifications.apply(lambda x: incompleteness(x.annotatorRationales, x.machineRationales),  axis=1)
    if verbose: print("mean incorrect set: %f" % incompleteness_incorrect.mean() if len(incorrect_classifications) > 0 else 0)

    if logits_sentences != []:
        dataset["logits_sentences"] = logits_sentences.tolist()
    if mrs_k != []:
        dataset["machineRationalesK"] = mrs_k
    return dataset
