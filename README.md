# Machine-annotated rationales






The git repo for Machine-annotated Rationales: Faithfully Explaining Text Classification.

E. Herrewijnen, J. Mense, D. Nguyen & F. Bex (2021) Machine-annotated Rationales: Faithfully Explaining Text Classification. Explainable Agency in AI Workshop (AAAI 2021)

## Steps:
    1. Gather data from https://www.cs.jhu.edu/~ozaidan/rationales/ and distribute  over data/ (dev, train, evaluation) folders.
    2. Update paths in data_preprocessing.py and run the file
    3. Run models.py
    

